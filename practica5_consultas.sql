USE practica5;

/*
  CONSULTA 1:
*/
-- c1: Composiciones que tienen presidentes
SELECT 
  * 
FROM composicion c 
WHERE c.cargo='Presidente';

-- final: Nombres de los presidentes
SELECT
  m.nombre_m  
FROM (SELECT * FROM composicion c WHERE c.cargo='Presidente') c1 
JOIN miembro m 
USING (dni);


/*
  CONSULTA 2: 
*/

-- c1: Composiciones que tienen gerentes
SELECT 
  * 
FROM composicion c
WHERE c.cargo='Gerente';

-- final: Direcci�n de las federaciones que tienen gerentes
SELECT 
  DISTINCT f.direccion 
FROM (SELECT * FROM composicion c WHERE c.cargo='Gerente') c1 
JOIN federacion f
USING(nombre);

/*
  CONSULTA 3:
*/

-- c1: Nombre de las composiciones que tienen Asesores T�cnicos
SELECT 
  DISTINCT c.nombre 
FROM composicion c 
WHERE c.cargo='Asesor T�cnico';


-- final: Nombre de las federaciones que no tienen asesores t�cnicos
SELECT 
  DISTINCT f.nombre
FROM federacion f 
LEFT JOIN (SELECT DISTINCT c.nombre FROM composicion c WHERE c.cargo='Asesor T�cnico') c1 
USING (nombre)
WHERE c1.nombre IS NULL;

/*
  CONSULTA 4:
*/
  -- denominador: todos los cargos de las composiciones
  SELECT DISTINCT cargo FROM composicion;

  -- numerador: Todas las composiciones con sus cargos
  SELECT nombre, cargo FROM composicion;

  -- N�mero de cargos por composici�n
  SELECT nombre, COUNT(*) n FROM (SELECT DISTINCT nombre, cargo FROM composicion) c1 GROUP BY nombre;

  -- final: Composiciones que tienen todos los cargos
  SELECT 
    nombre
  FROM (SELECT DISTINCT nombre, cargo FROM composicion) c1 
  GROUP BY nombre 
  HAVING COUNT(*)=(SELECT COUNT(DISTINCT cargo) FROM composicion);

 
/*
  CONSULTA 5:
*/

-- c1: Composiciones que tienen asesores t�cnicos
SELECT
  * 
FROM composicion c 
WHERE c.cargo='Asesor Tecnico';

-- c2: Composiciones que tienen psic�logos
SELECT 
  * 
FROM composicion c 
WHERE c.cargo='Psicologo';

-- final: Nombre de la composiciones que tienen asesores t�cnicos y psic�logos
SELECT DISTINCT c1.nombre FROM (
  SELECT * FROM composicion c WHERE c.cargo='Asesor Tecnico'
  ) c1
  JOIN 
  (
  SELECT * FROM composicion c WHERE c.cargo='Psicologo'
  ) c2
  ON c1.nombre=c2.nombre;