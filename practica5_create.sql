﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE practica5;
USE practica5;


CREATE OR REPLACE TABLE federacion (
  nombre varchar(100),
  direccion varchar(100),
  telefono varchar(12),
  PRIMARY KEY (nombre)
  );

CREATE OR REPLACE TABLE miembro (
  dni varchar(9),
  nombre_m varchar(100),
  titulacion varchar(100),
  PRIMARY KEY (dni)
);


CREATE OR REPLACE TABLE composicion (
  nombre varchar(100),
  dni varchar(9),
  cargo varchar(100),
  fecha_inicio date,
  PRIMARY KEY (nombre, dni)
);

ALTER TABLE composicion
  ADD CONSTRAINT fkcomposicion_federacion FOREIGN KEY (nombre)
  REFERENCES federacion(nombre);

ALTER TABLE composicion 
  ADD CONSTRAINT fkcomposicion_miembro FOREIGN KEY (dni)
  REFERENCES miembro (dni);